from selenium.webdriver.common.by import By

from ..abstract_solver import AbstractWordleSolver, ResultType


class AutoAbsurdle(AbstractWordleSolver):
    """Solver for Absurdle."""

    url = 'https://qntm.org/files/absurdle/absurdle.html'
    word_file='w.txt'

    def initialise_game(self):
        super().initialise_game()
        self.sleep()
        
    def __init__(self, start_word: str, ):
        super().__init__(start_word)
        
    @property
    def guess_table(self):
        return self.browser.find_element(By.CLASS_NAME, 'absurdle__guess-table')
        
    def get_results(self, row: int):
        print(f"Getting results for row {row}")
        row = self.guess_table.find_elements(By.TAG_NAME, 'tr')[row]
        def generator():
            for i, cell in enumerate(row.find_elements(By.TAG_NAME, 'td'), start=1):
                letter = cell.text
                class_name = cell.get_attribute("class")
                if class_name.endswith("inexact"):
                    result = ResultType.PRESENT
                elif class_name.endswith("wrong"):
                    result = ResultType.ABSENT
                elif class_name.endswith("exact"):
                    result = ResultType.CORRECT
                elif class_name.endswith("empty"):
                    result = ResultType.CORRECT
                else:
                    raise ValueError(f"Unrecognised result cell class: {class_name}")
                yield (letter, result)
                if i == 5:
                    break
        return list(generator())
