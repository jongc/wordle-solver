from typing import List, Tuple

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException

from ..abstract_solver import AbstractWordleSolver, ResultType

class AutoWordle(AbstractWordleSolver):
    
    url = 'https://www.nytimes.com/games/wordle/index.html#wordle-app-game'
    word_file='w.txt'

    def initialise_game(self):
        super().initialise_game()
        self.sleep()
        self.click_game_icon()
        
    def __init__(self, start_word: str, ):
        super().__init__(start_word)
        
    def get_shadow_root(self, element):
        return self.browser.execute_script("return arguments[0].shadowRoot", element)

    @property
    def game_theme_manager_root(self):
        try:
            game_app = WebDriverWait(self.browser, 10).until(
                EC.presence_of_element_located((By.ID, 'wordle-app-game'))
            )
        except (NoSuchElementException, TimeoutException):
            print("ERROR: Unable to find 'game-app' element")
            raise
        board = self.browser.execute_script("return arguments[0].shadowRoot.getElementById('board')", game_app)
        game_theme_manager_root = self.browser.execute_script("return arguments[0].shadowRoot", game_app)
        return game_theme_manager_root

    def click_terms_continue_button(self):
        print("Searching for terms continue button...")
        button = WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "purr-blocker-card__button"))
        )
        print("Found button. Clicking...")
        button.click()
    
    def click_play_button(self):
        print("Searching for play button...")
        button = WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, "//button[@data-testid='Play']"))
        )
        print("Found button. Clicking...")
        button.click()

    def close_how_to_play(self):
        print("Searching for close button for How to Play window...")
        button = WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, "//button[@aria-label='Close']"))
        )
        print("Found button. Clicking...")
        button.click()

    def click_game_icon(self):
        self.click_terms_continue_button()
        self.click_play_button()
        self.close_how_to_play()
    
    def get_results(self, row: int) -> List[Tuple[str, ResultType]]:
        """Get the results from a word."""
        print(f"Getting results for row {row}")

        game_row = self.browser.find_element(By.XPATH, f"//div[@aria-label='Row {row+1}']")
        game_tiles = game_row.find_elements(By.XPATH, f".//div[@aria-roledescription='tile']")

        print(f"Found {len(game_tiles)} tiles")

        def generator():
            for i, t in enumerate(game_tiles, start=1):
                aria_label = t.get_attribute("aria-label")
                letter = aria_label.split(",")[1].strip()
                result_str = t.get_attribute("data-state")
                print(f"Tile {i}: {letter} - {result_str} ({aria_label})")
                if result_str == "absent":
                    result_type = ResultType.ABSENT
                elif result_str == "present":
                    result_type = ResultType.PRESENT
                elif result_str == "correct":
                    result_type = ResultType.CORRECT
                elif result_str == "empty":
                    raise ValueError("Empty result type")
                else:
                    raise ValueError(f"Unrecognised result type: {result_str}")
                yield (letter, result_type)
        return list(generator())
