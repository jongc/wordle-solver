import time
from abc import ABCMeta, abstractmethod
from typing import List, Tuple, Optional
from enum import Enum

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, TimeoutException

from wordle_solver.guesser import WordleGuesser


class ResultType(Enum):
    """Represents the type of result."""
    ABSENT = "⬜"
    PRESENT = "🟨"
    CORRECT = "🟩"

    
class AbstractWordleSolver(metaclass=ABCMeta):
    """Abstract class for a Wordle solver."""
    url: str
    word_file: str = "w.txt"
    verbose: bool = False
    typing_delay: Optional[float] = 0.1    
        
    def __init__(self, start_word: str, ):
        self.start_word = start_word
        self.current_row = 0
        
    def open_browser(self):
        self.browser: webdriver.Chrome = webdriver.Chrome()
    
    def close_browser(self):
        self.browser.close()
        delattr(self, 'browser')

    def initialise_game(self):
        self.open_browser()
        self.browser.get(self.url)
        self.guesser = WordleGuesser(word_file=self.word_file, verbose=self.verbose)
    
    def send_key(self, key):
        actions = ActionChains(self.browser)
        actions.send_keys(key)
        actions.perform()

    def send_word(self, word): 
        for letter in word:
            self.send_key(letter)
            if self.typing_delay:
                self.sleep(self.typing_delay)
        self.send_key(Keys.ENTER)

    def new_tab(self):
        self.send_key(Keys.COMMAND + 't')
        
    @abstractmethod
    def get_results(self, row:int) -> List[Tuple[str, ResultType]]:
        """Get the results from a word."""

    @staticmethod
    def get_letters_by_type(t: ResultType, results) -> str:
        value = ''.join([r[0] for r in results if r[1] == t])
        return value.lower()

    def process_grey_letters(self, results):
        self.guesser.grey_letters(letters=self.get_letters_by_type(ResultType.ABSENT, results))
        
    def process_yellow_letters(self, results):
        for x in [{'letter': r[0], 'position': i+1} for i, r in enumerate(results) if r[1] == ResultType.PRESENT]:
            self.guesser.yellow_letter(**x)        
            
    def process_green_letters(self, results):
        for x in [{'letter': r[0], 'position': i+1} for i, r in enumerate(results) if r[1] == ResultType.CORRECT]:
            self.guesser.green_letter(**x)       
            
    def clean_results(self, results):
        return [(r[0].lower(),r[1]) for r in results]
            
    def process_results(self, results):
        results = self.clean_results(results)
        self.process_grey_letters(results)
        self.process_yellow_letters(results)
        self.process_green_letters(results)

    ## TODO: Break this out into a word selector class.
    def get_next_word(self):
        try:
            return self.guesser.suggestions_without_repeats[0]
        except IndexError:
            return self.guesser.words[0]
        
    def winner(self, results) -> bool:
        return len([r[0] for r in results if r[1] == ResultType.CORRECT]) == 5
        
    @staticmethod
    def result_row_to_emojis(results):
        return ''.join([x[1].value for x in results])

    def _run_game(self):
        print(f"Starting...")
        next_word = self.start_word
        last_result = []
        all_results = []

        while not self.winner(last_result):
            print(f"Current word: {self.current_row}")
            print(f"Trying word: {next_word}")
            self.sleep(1)
            self.send_word(next_word)
            self.sleep(2)
            last_result = self.get_results(self.current_row)
            print(self.result_row_to_emojis(last_result))
            all_results.append(last_result)
            self.process_results(last_result)
            next_word = self.get_next_word()
            self.current_row += 1
        
        print(f"Word: {next_word}")
        print(f"Completed in {self.current_row} rows.")
        
        for r in all_results:
            print(self.result_row_to_emojis(r))


    def run(self, close_broswer_on_exception: bool = True):
        self.initialise_game()
        try:
            self._run_game()
            time.sleep(5)
            self.close_browser()
        except Exception as e:
            if close_broswer_on_exception:
                self.close_browser()
            raise

    @staticmethod
    def sleep(secs=2):
        time.sleep(secs)
