"""Guesser class for Wordle solver."""

from typing import List


class WordleGuesser:
    
    words: List[str]
    verbose: bool
    excludes: str
    yellows: str
    print_threshold: int = 10
    
    def __init__(self, word_file: str, verbose: bool = False):
        with open(word_file) as f:
            self.words = f.read().splitlines()
        self.verbose = verbose
        self.status()
        self.excludes = []
        self.yellows = []
        self.green_letter_positions: Dict[int, str] = {}  # Letter, Position

    def status(self, suffix: str = ""):
        """Display current status."""
        if len(self.words) < 10:
            words = ": " + (', '.join(self.words))
        else:
            words = ""
        
        print(f"{len(self.words)} words remaining {suffix}{words}")

    @property
    def green_letters(self) -> List[str]:
        return list(self.green_letter_positions.values())
        
    def get_unsolved_letters(self, word: str) -> str:
        if not self.green_letter_positions:
            return word
        positions = sorted(list(self.green_letter_positions.keys()), reverse=True)
        word_list = list(word)
        for pos in positions:
            del word_list[pos]
        value = "".join(word_list)
        return value
        
    def contains_excluded_letters(self, word: str, exclude: str) -> bool:
        """Check whether a word contains excluded letters."""
        for letter in exclude:
            if letter in self.get_unsolved_letters(word):
                return True
        return False
        
    def grey_letters(self, letters: str):
        """Add a set of grey letters."""

        if self.verbose:
            print(f"Adding grey letters: '{letters}'")
        
        def generator():
            """Generate words that are not eliminated."""
            
            # Check whether words contain grey letters in non-green locations.
            for word in self.words:
                if self.contains_excluded_letters(word, letters):
                    if self.verbose:
                        print(f"Eliminating '{word}' - contains excluded letters: {letters}")
                else:
                    yield word

        self.words = list(generator())
        self.status(suffix=f"after processing grey letters {', '.join(letters)}")
        
    def yellow_letter(self, letter: str, position: int):
        """Add a yellow letter."""
        position = position - 1
        if self.verbose:
            print(f"Adding yellow letter: '{letter}' at position {position}")
        def generator():
            for word in self.words:
                if word[position] == letter:
                    continue
                if not letter in word:
                    continue
                yield word
        self.words = list(generator())
        self.status(suffix=f"after processing yellow letter {letter} at position {position}")

    def green_letter(self, letter: str, position: int):
        """Add a green letter."""
        position = position - 1
        if self.verbose:
            print(f"Adding green letter: '{letter}' at position {position}")
        def generator():
            for word in self.words:
                if word[position] == letter:
                    yield word
        self.words = list(generator())
        self.green_letter_positions[position] = letter
        self.status(suffix=f"after processing green letter {letter} at position {position}")
        
    @property
    def suggestions_without_repeats(self):
        """Get some suggestions that don't contain repeats."""
        def generator():
            for word in self.words:
                if len(word) > len(set(word)):
                    continue
                yield word
        return list(generator())
