# Wordle Solver

Solver for Wordle and Absurdle

## Requirements

- Selenium: https://www.selenium.dev/selenium/docs/api/py/index.html
- Chromium: https://www.chromium.org/getting-involved/download-chromium/
  
Selenium can be installed with `pip install selenium`

Chromium must be installed and available to run on the system path.

Requirements can be installed using the following commands:

```bash
pip install invoke
inv setup
inv install_chromium  # Requies npx to be installed
```

## Running the Solver

### From the Command Line

To run the solver from the command line, use the following command:

```bash
inv solve_wordle
inv solve_wordle --start-word=apple
```

To run the Absurdle solver, use the following command:

```bash
inv solve_absurdle
inv solve_absurdle --start-word=apple
```

### From Jupyter Notebook

First start a Jupyter notebook server:

```bash
inv start-notebook
```

Then open the `Wordle-Solver.ipynb` notebook.
