from pathlib import Path

from invoke import task

PROJECT_DIR = Path(__file__).parent
VENV = PROJECT_DIR / ".venv"
VENV_PYTHON = VENV / "bin" / "python"

@task
def setup(c):
    """Setup the project."""
    print("Setting up")
    if not VENV.exists():
        print("Creating virtual environment")
        c.run(f"python -m venv {VENV}")
    print("Installing dependencies")
    c.run(f"{VENV_PYTHON} -m pip install -r requirements.txt")
    print("Done")

@task
def install_chromium(c):
    """Install Chromium browser."""
    print("Installing Chromium")
    c.run("npx @puppeteer/browsers install chrome@stable")
    print("Done")

@task
def start_notebook(c):
    """Start Jupyter Notebook server."""
    print("Starting Jupyter Notebook")
    c.run(f"{VENV_PYTHON} -m jupyter notebook")

@task
def solve_wordle(c, start_word="table"):
    """Solve Wordle using Chrome browser."""
    from wordle_solver.solvers.wordle import AutoWordle
    solver = AutoWordle(start_word)
    solver.run()

@task
def solve_absurdle(c, start_word="table"):
    """Solve Absurdle using Chrome browser."""
    from wordle_solver.solvers.absurdle import AutoAbsurdle
    solver = AutoAbsurdle(start_word)
    solver.run()
